var _WM_APP_PROPERTIES = {
  "activeTheme" : "material",
  "dateFormat" : "",
  "defaultLanguage" : "en",
  "displayName" : "RandomGen",
  "homePage" : "Main",
  "name" : "RandomGen",
  "platformType" : "WEB",
  "supportedLanguages" : "en",
  "timeFormat" : "",
  "type" : "APPLICATION",
  "version" : "1.0"
};